Source: paulstretch
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Robin Gareus <robin@gareus.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper-compat (= 10),
 fluid,
 libaudiofile-dev,
 libfftw3-dev,
 libfltk1.1-dev | libfltk1.3-dev | libfltk-dev,
 libjack-jackd2-dev | libjack-dev,
 libmad0-dev,
 libmxml-dev,
 libsamplerate0-dev,
 libvorbis-dev,
 libvorbisidec-dev,
 libz-dev | zlib1g-dev,
 portaudio19-dev
Homepage: http://hypermammut.sourceforge.net/paulstretch/
Standards-Version: 3.9.8
Vcs-Git: https://salsa.debian.org/multimedia-team/paulstretch.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/paulstretch

Package: paulstretch
Architecture: any
Depends:
 jackd,
 ${misc:Depends},
 ${shlibs:Depends}
Description: Extreme sound time-stretch
 This is a program for stretching the audio.
 It is suitable only for extreme sound stretching of the audio (like 50x)
 and for applying special effects by "spectral smoothing" the sounds.
 It can transform any sound/music to a texture.
 .
 It produces high quality extreme sound stretching. While most sound
 stretching software sounds bad when trying to stretch the sounds a lot,
 this one is optimized for extreme sound stretching.
 .
 The stretch amount is unlimited.
 You can play the stretched sound in real-time (including the possibility
 to "freeze" the sound) or you can render the whole sound or a
 part of it to audio files.
 .
 It has many post-processing effects, like: filters, pitch/frequency shifters
 Support for WAV, OGG VORBIS files and MP3 files.
